package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import model.User;
import repository.UserRepository;

@Component
public class InitialUserCreation implements ApplicationRunner {

	@Autowired
	private UserRepository objUserRepository;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		User user = new User();
		user.setUsername("PINNACLESEVEN");
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); 
		user.setPassword(passwordEncoder.encode("P7@123"));
		user.setRole("ROLE_ADMIN");
		user.setCountry("IND");
		user.setFullName("Pinnacle Seven Technologies");
		
		objUserRepository.save(user);

	}

}