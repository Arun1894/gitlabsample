package controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.MyEmailService;
import service.OtpService;

@Controller
public class OtpController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public OtpService otpService;
	
	@Autowired
	public MyEmailService myEmailService;

	@GetMapping("/generateOtp")
	public String generateOtp(){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
		String username = auth.getName();
		
		Integer otp = otpService.generateOTP(username);
		 
		logger.info("OTP : "+otp);
		
		String message = otp.toString();
		
		myEmailService.sendOtpMessage("aruncse1010@gmail.com", "OTP - PinnacleSeven", message);
		
		return "otppage";
	}
	
	@RequestMapping(value ="/validateOtp", method = RequestMethod.GET)
	public @ResponseBody String validateOtp(@RequestParam("otpnum") int otpnum){
		
		final String SUCCESS = "Entered Otp is valid";
		
		final String FAIL = "Entered Otp is NOT valid. Please Retry!";
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
		String username = auth.getName();
		
		logger.info(" Otp Number : "+otpnum);
		
		//Validate the Otp 
		if(otpnum >= 0){
			int serverOtp = otpService.getOtp(username);
			
			if(serverOtp > 0){
				System.out.println(serverOtp);
				if(otpnum == serverOtp){
					otpService.clearOTP(username);
					return ("Entered Otp is valid");
				}else{
					return FAIL;	
				}
			}else {
				return FAIL;			
			}
		}else {
			return FAIL;	
		}
	}
}
